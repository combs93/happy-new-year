/* frameworks */
//=include ../../node_modules/svg4everybody/dist/svg4everybody.min.js

// the main code
svg4everybody();

// burger
if (document.querySelectorAll('.header__burger').length > 0) {
    document.querySelector('.header__burger').onclick = function(e) {
        this.closest('.l-wrapper').classList.add('menu-is-active');
    }
    document.querySelector('.header-mobile__close').onclick = function(e) {
        this.closest('.l-wrapper').classList.remove('menu-is-active');
    }
}

// header scroll
window.addEventListener('scroll', function(){
    if(window.scrollY > 10){
        document.querySelector('.header').classList.add('is-scrolled');
    }
    else {
        document.querySelector('.header').classList.remove('is-scrolled');
    }
});